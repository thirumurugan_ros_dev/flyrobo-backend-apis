#!/usr/env/bin python3

"""curd.py: Flask-REST API for Backend."""

__author__      = "Thirumurugan.C"
__email__   = "thirumurugan.chokkalingam@infovision.com"

from types import MethodType
from flask import Flask, app, jsonify, request, json
from flask_pymongo import PyMongo
import datetime

utc_tmsp = datetime.datetime.utcnow()
print(utc_tmsp)
#utc = utc_tmsp.isoformat()
#json_utc = json.dumps(utc)


app = Flask(__name__)

#app.config["MONGO_DBNAME"] = "FlyRoboDatabase"
app.config["MONGO_URI"] = "mongodb://localhost:27017/FlyRoboDatabase"


mongo = PyMongo(app)

class Edgedev():
    global utc_tmsp
    #global utc
    #global json_utc
    @app.route('/edgedev', methods=['GET'])

    def get_edge_dev_list():
        edge = mongo.db.edge_devc
        #print(edge)
        output = []
        for i in edge.find():
            output.append({'user_id': i['user_id'], 'correlation_id': i['correlation_id'], 'name': i['name'], 'ip_addr': i['ip_addr'], 'mac_addr': i['mac_addr'], 'edge_dev_username': i['edge_dev_username'], 'edge_dev_pass': i['edge_dev_pass'], 'assoc_drones': i['assoc_drones'], 'assoc_chrgs':i['assoc_chrgs'], 'is_active': i['is_active'], 'is_deleted': i['is_deleted'], 'created_by': i['created_by'], 'modified_by': i['modified_by'], 'created_tmsp': i['created_tmsp'], 'modified_tmsp': i['modified_tmsp'] })
        return jsonify({'result': output})

    @app.route('/edgedev', methods=['POST'])

    def add_edge_dev():
        edge = mongo.db.edge_devc
        #json_data = app.request.json
    
        
        user_id = request.json['user_id']
        correlation_id = request.json['correlation_id']
        name = request.json['name']
        ip_addr = request.json['ip_addr']
        mac_addr = request.json['mac_addr']
        edge_dev_username = request.json['edge_dev_username']
        edge_dev_pass = request.json['edge_dev_pass']
        assoc_drones = request.json['assoc_drones']
        assoc_chrgs = request.json['assoc_chrgs']
        is_active = request.json['is_active']
        is_deleted = request.json['is_deleted']
        created_by = request.json['created_by']
        modified_by = request.json['modified_by']
        created_tmsp = request.json['created_tmsp']
        modified_tmsp = request.json['modified_tmsp']
         #print(edge_dev_name)
        #print(edge_dev_pass)

        edge_dev_id = edge.insert({'user_id': user_id, 'correlation_id': correlation_id, 'name': name, 'ip_addr': ip_addr, 'mac_addr': mac_addr,'edge_dev_username':edge_dev_username,'edge_dev_pass':edge_dev_pass, 'assoc_drones': assoc_drones,'assoc_chrgs':assoc_chrgs,'is_active':is_active, 'is_deleted':is_deleted,'created_by':created_by,'modified_by':modified_by,'created_tmsp':created_tmsp,'modified_tmsp':modified_tmsp})
        new_edge_id = edge.find_one({'_id': edge_dev_id})

        output = {'user_id': new_edge_id['user_id'], 'correlation_id': new_edge_id['correlation_id'],'name': new_edge_id['name'], 'ip_addr': new_edge_id['ip_addr'], 'mac_addr': new_edge_id['mac_addr'],'edge_dev_username': new_edge_id['edge_dev_username'],'edge_dev_pass': new_edge_id['edge_dev_pass'],'assoc_drones': new_edge_id['assoc_drones'],'assoc_chrgs': new_edge_id['assoc_chrgs'],'is_active': new_edge_id['is_active'],'is_deleted': new_edge_id['is_deleted'],'created_by': new_edge_id['created_by'],'modified_by': new_edge_id['modified_by'],'created_tmsp': new_edge_id['created_tmsp'],'modified_tmsp': new_edge_id['modified_tmsp']}
        return jsonify({'result': output, 'tmsp': datetime.datetime.utcnow()})


    @app.route('/edgedev1', methods=['POST'])
    def add_drone_dev():
        
    
        dronee = mongo.db.drone_devc


        user = request.json['user']
        
       
        drone_id = dronee.insert({'user': user})
        new_drone_id = dronee.find_one({'_id': drone_id})

        final = {'user': new_drone_id['user']}
        return jsonify({'result': final, 'http-response': 200, 'tmsp': datetime.datetime.utcnow(),'resp-msg': "success"})






if __name__ == '__main__':
    app.run(debug=True)
    Edgedev()