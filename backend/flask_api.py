#!/usr/env/bin python3

"""flask_app.py: Flask-REST API for Backend."""

__author__      = "Thirumurugan.C"
__email__   = "thirumurugan.chokkalingam@infovision.com"
__last_modified__ = "00-01-2022"
__project__ = "FlyRobo"


from types import MethodType
from flask import Flask, app, jsonify, request, json 
import json_logging
import bson
from flask_pymongo import PyMongo
from pymongo import MongoClient
import datetime, logging, sys  
#from keycloak import KeycloakAdmin, KeycloakOpenID
from logging import FileHandler, WARNING
from flask_swagger_ui import get_swaggerui_blueprint 
from flask_oidc import OpenIDConnect

#app initialize
app = Flask(__name__)
#DB config - uri string to connect to localhost
#app.config["MONGO_URI"] = "mongodb://localhost:27017/FlyRoboDatabase"
#DB config - uri string to connect to cloud
app.config["MONGO_URI"] = "mongodb://flyrobo:flyrobo@52.89.181.53:27017/flyrobo"
mongo = PyMongo(app)
#OIDC obj
#oidc = OpenIDConnect(app)
#json logging
json_logging.init_flask(enable_json=True)
json_logging.init_request_instrument(app)
#init the logger
logger = logging.getLogger("flyrobo-logger")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

#error Logging
file_handler = FileHandler('errorlog.txt')
file_handler.setLevel(WARNING)
app.logger.addHandler(file_handler)

utc_tmsp = datetime.datetime.utcnow()
#swagger configs
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGER_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name' : "Flyrobo backend API's"
    }
)
app.register_blueprint(SWAGGER_BLUEPRINT, url_prefix = SWAGGER_URL)

class Configuration():
    '''def __init__(self, data):
        self.client = MongoClient("mongodb://localhost:27017/FlyRoboDatabase")
        database = data['FlyRoboDatabase']
        collection = data['collection']
        cursor = self.client[database]
        self.collection = cursor[collection]
        self.data = data'''

    def __init__(self) -> None:
        pass
   
        ''' GET Requests for configuration module'''

    @app.route('/getDroneList', methods=['GET'])
    def get_dronedev_list():
        drone_dev_col = mongo.db.edge_devc
        drone_dev_json = [] 
        if drone_dev_col is None or drone_dev_col == {}:
            return jsonify({"Error": "Please provide connection information"})
        if drone_dev_col.find({}):
            for name in drone_dev_col.find({}).sort("assoc_drones"):
                try:
                    drone_dev_json.append({"assoc_drones": name["assoc_drones"]})
                except KeyError:
                    return "something wrong"
        #return json.dumps(names_json)
        return jsonify({'assoc_drone_list': drone_dev_json,
        '$access-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        '$correlation_id': "1",
        '$http-response': 200, 
        '$tmsp': datetime.datetime.utcnow(),
        '$resp-msg': "success"})


    @app.route('/getChrgrList', methods=['GET'])
    def get_chrgrdev():
        chrgr_dev_col = mongo.db.drone_devc
        chrgr_dev_json = [] 
        if chrgr_dev_col.find({}):
            for name in chrgr_dev_col.find({}).sort("assoc_chrgs"):
                chrgr_dev_json.append({"assoc_chrgs": name["assoc_chrgs"]})
        #return json.dumps(names_json)
        return jsonify({'assoc_chrgs_list': chrgr_dev_json,
        '$access-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        '$correlation_id': "1",
        '$http-response': 200, 
        '$tmsp': datetime.datetime.utcnow(),
        '$resp-msg': "success"})


    @app.route('/getMapList', methods=['GET'])
    def get_map():
        map_col = mongo.db.map
        map_json = [] 
        if map_col.find({}):
            for name in map_col.find({}).sort("assoc_map"):
                map_json.append({"assoc_chrgr": name["assoc_map"]})
        #return json.dumps(names_json)
        return jsonify({'assoc_map_list': map_json,
        '$access-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        '$correlation_id': "1",
        '$http-response': 200, 
        '$tmsp': datetime.datetime.utcnow(),
        '$resp-msg': "success"})

    @app.route('/edgedev', methods=['GET'])
    def get_edgedev():
        edge_dev_col = mongo.db.drone_devc
        edge_dev_json = [] 
        if edge_dev_col.find({}):
            for name in edge_dev_col.find({}).sort("assoc_edge_dev"):
                edge_dev_json.append({"assoc_edge_dev": name["assoc_edge_dev"]})
        #return json.dumps(names_json)
        return jsonify({'assoc_edgedev_list': edge_dev_json,
        '$access-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        '$correlation_id': "1",
        '$http-response': 200, 
        '$tmsp': datetime.datetime.utcnow(),
        '$resp-msg': "success"})

    @app.route('/getCycleCount', methods=['GET'])
    def get_cycle_count():
        cycle_count_col = mongo.db.cycle_count_db
        cycle_count_json = [] 
        if cycle_count_col.find({}):
            for name in cycle_count_col.find({}).sort("cycle_count"):
                cycle_count_json.append({"cycle_count": name["cycle_count"]})
        #return json.dumps(names_json)
        return jsonify({'assoc_drone_list': cycle_count_json,
        '$access-token': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        '$correlation_id': "1",
        '$http-response': 200, 
        '$tmsp': datetime.datetime.utcnow(),
        '$resp-msg': "success"})


# For Testing *----do not use the below api----------*
    #@app.route('/cyclecount', methods=['GET'])
    def get_cycle_count():
        edge = mongo.db.charger_devc
        output = []
        for i in edge.find():
            output.append({'user_id': i['user_id'], 
            'correlation_id': i['correlation_id'], 
            'name': i['name'], 
            'ip_addr': i['ip_addr'], 
            'mac_addr': i['mac_addr'], 
            'edge_dev_username': i['edge_dev_username'], 
            'edge_dev_pass': i['edge_dev_pass'], 
            'assoc_drones': i['assoc_drones'], 
            'assoc_chrgs':i['assoc_chrgs'], 
            'is_active': i['is_active'], 
            'is_deleted': i['is_deleted'], 
            'created_by': i['created_by'], 
            'modified_by': i['modified_by'], 
            'created_tmsp': i['created_tmsp'], 
            'modified_tmsp': i['modified_tmsp'] })
        return jsonify({'result': output})


    ''''POST REQUEST for configuration module'''
    #Do not use the below API *----Testing only----*
    #@app.route('/edgedev', methods=['POST'])
    def add_edge_dev():
        edge_db = mongo.db.edge_devc
        request_data = request.get_json()

        for req in request_data:

            user_id = req['user_id']
            correlation_id = req['correlation_id']
            name = req['name']
            ip_addr = req['ip_addr']
            mac_addr = req['mac_addr']
            edge_dev_username = req['edge_dev_username']
            edge_dev_pass = req['edge_dev_pass']
            assoc_drones = req['assoc_drones']
            assoc_chrgs = req['assoc_chrgs']
            is_active = req['is_active']
            is_deleted = req['is_deleted']
            created_by = req['created_by']
            modified_by = req['modified_by']
            created_tmsp = req['created_tmsp']
            modified_tmsp = req['modified_tmsp']
       

        edge_dev_id = edge_db.insert({'user_id': user_id, 
        'correlation_id': correlation_id, 'name': name,
        'ip_addr': ip_addr, 
        'mac_addr': mac_addr,
        'edge_dev_username':edge_dev_username,
        'edge_dev_pass':edge_dev_pass, 
        'assoc_drones': assoc_drones,
        'assoc_chrgs':assoc_chrgs,
        'is_active':is_active, 
        'is_deleted':is_deleted,
        'created_by':created_by,
        'modified_by':modified_by,
        'created_tmsp':created_tmsp,
        'modified_tmsp':modified_tmsp})

        new_edge_id = edge_db.find_one({'_id': edge_dev_id})

        output = {'user_id': new_edge_id['user_id'], 
        'correlation_id': new_edge_id['correlation_id'],
        'name': new_edge_id['name'], 
        'ip_addr': new_edge_id['ip_addr'], 
        'mac_addr': new_edge_id['mac_addr'],
        'edge_dev_username': new_edge_id['edge_dev_username'],
        'edge_dev_pass': new_edge_id['edge_dev_pass'],
        'assoc_drones': new_edge_id['assoc_drones'],
        'assoc_chrgs': new_edge_id['assoc_chrgs'],
        'is_active': new_edge_id['is_active'],
        'is_deleted': new_edge_id['is_deleted'],
        'created_by': new_edge_id['created_by'],
        'modified_by': new_edge_id['modified_by'],
        'created_tmsp': new_edge_id['created_tmsp'],
        'modified_tmsp': new_edge_id['modified_tmsp']}
        return jsonify({'result': output, 
        'tmsp': datetime.datetime.utcnow()})

    #'''ADDING EDGE DEVICE TO DATABASE'''
    @app.route('/edgedev', methods=['POST'])
    def add_edge_dev():
        edge_db = mongo.db.edge_devc
        logger.info("test log statement")
        logger.info("test log statement with extra props", extra={'props': {"extra_property": 'extra_value'}})
        correlation_id = json_logging.get_correlation_id()
        request_data = request.get_json()

        for req in request_data:
            user_id = 1
            correlation_id = correlation_id
            username = req['username']
            ip_addr = req['ip_addr']
            mac_addr = req['mac_addr']
            edge_dev_username = req['edge_dev_username']
            edge_dev_pass = req['edge_dev_pass']
            assoc_drones = req['assoc_drones']
            assoc_chrgs = req['assoc_chrgs']
            is_active = True
            is_deleted = False
            created_by = "user"
            modified_by = "user"
            created_tmsp = datetime.datetime.utcnow()
            modified_tmsp = "08-01-2022 03:15:10"
        
       
            edge_dev_id = edge_db.insert({'user_id': user_id, 
            'correlation_id': correlation_id, 'username': username,
            'ip_addr': ip_addr, 
            'mac_addr': mac_addr,
            'edge_dev_username':edge_dev_username,
            'edge_dev_pass':edge_dev_pass, 
            'assoc_drones': assoc_drones,
            'assoc_chrgs':assoc_chrgs,
            'is_active':is_active, 
            'is_deleted':is_deleted,
            'created_by':created_by,
            'modified_by':modified_by,
            'created_tmsp':created_tmsp,
            'modified_tmsp':modified_tmsp})

            new_edge_id = edge_db.find_one({'_id': edge_dev_id})

            output = {'user_id': new_edge_id['user_id'], 
            'correlation_id': new_edge_id['correlation_id'],
            'username': new_edge_id['username'], 
            'ip_addr': new_edge_id['ip_addr'], 
            'mac_addr': new_edge_id['mac_addr'],
            'edge_dev_username': new_edge_id['edge_dev_username'],
            'edge_dev_pass': new_edge_id['edge_dev_pass'],
            'assoc_drones': new_edge_id['assoc_drones'],
            'assoc_chrgs': new_edge_id['assoc_chrgs'],
            'is_active': new_edge_id['is_active'],
            'is_deleted': new_edge_id['is_deleted'],
            'created_by': new_edge_id['created_by'],
            'modified_by': new_edge_id['modified_by'],
            'created_tmsp': new_edge_id['created_tmsp'],
            'modified_tmsp': new_edge_id['modified_tmsp']}
            return jsonify({'result': output, 
            'tmsp': datetime.datetime.utcnow()})

    #ADDING DRONE TO THE DATABASE
    @app.route('/dronedev', methods=['POST'])
    def add_drone_dev():
        drone_db = mongo.db.drone_devc
        request_data = request.get_json()
        for req in request_data:
            user_id = "1"
            correlation_id = "01"
            username = req['username']
            ip_addr = req['ip_addr']
            mac_addr = req['mac_addr']
            drone_name = req['drone_name']
            assoc_edge_dev = req['assoc_edge_dev']
            assoc_map = req['assoc_map']
            assoc_chrgs = req['assoc_chrgs']
            is_active = True
            is_deleted = False
            created_by = "user"
            modified_by = "user"
            created_tmsp = datetime.datetime.utcnow()
            modified_tmsp = "08-01-2022 03:15:10"
        
       
            drone_dev_id = drone_db.insert({'user_id': user_id, 
            'correlation_id': correlation_id, 
            'username': username,
            'ip_addr': ip_addr, 
            'mac_addr': mac_addr,
            'drone_name':drone_name,
            'assoc_edge_dev': assoc_edge_dev,
            'assoc_map': assoc_map,
            'assoc_chrgs':assoc_chrgs,
            'is_active':is_active, 
            'is_deleted':is_deleted,
            'created_by':created_by,
            'modified_by':modified_by,
            'created_tmsp':created_tmsp,
            'modified_tmsp':modified_tmsp})

            new_drone_id = drone_db.find_one({'_id': drone_dev_id})

            output = {'user_id': new_drone_id['user_id'], 
            'correlation_id': new_drone_id['correlation_id'],
            'username': new_drone_id['username'], 
            'ip_addr': new_drone_id['ip_addr'], 
            'mac_addr': new_drone_id['mac_addr'],
            'drone_name': new_drone_id['drone_name'],
            'assoc_edge_dev': new_drone_id['assoc_edge_dev'],
            'assoc_map': new_drone_id['assoc_map'],
            'assoc_chrgs': new_drone_id['assoc_chrgs'],
            'is_active': new_drone_id['is_active'],
            'is_deleted': new_drone_id['is_deleted'],
            'created_by': new_drone_id['created_by'],
            'modified_by': new_drone_id['modified_by'],
            'created_tmsp': new_drone_id['created_tmsp'],
            'modified_tmsp': new_drone_id['modified_tmsp']}
            return jsonify({'result': output, 
            'tmsp': datetime.datetime.utcnow()})

    
    #ADDING CHARGERS TO THE DATABASE
    @app.route('/chargerdev', methods=['POST'])
    def add_chrgr_dev():
        chrgr_db = mongo.db.charger_devc
        request_data = request.get_json()
        for req in request_data:

            user_id = "2"
            correlation_id = "02"
            username = request.json['username']
            ip_addr = request.json['ip_addr']
            mac_addr = request.json['mac_addr']
            assoc_edge_dev = request.json['assoc_edge_dev']
            assoc_drone = request.json['assoc_drone']
            is_active = True
            is_deleted = False
            created_by = "user"
            modified_by = "user"
            created_tmsp = datetime.datetime.utcnow()
            modified_tmsp = "08-01-2022 03:15:10"
            
        
            chrgr_dev_id = chrgr.insert({'user_id': user_id, 
            'correlation_id': correlation_id, 
            'username': username,
            'ip_addr': ip_addr, 
            'mac_addr': mac_addr,
            'assoc_edge_dev': assoc_edge_dev,
            'assoc_drone': assoc_drone,
            'is_active':is_active, 
            'is_deleted':is_deleted,
            'created_by':created_by,
            'modified_by':modified_by,
            'created_tmsp':created_tmsp,
            'modified_tmsp':modified_tmsp})

            new_chrgr_id = chrgr_db.find_one({'_id': chrgr_dev_id})

            output = {'user_id': new_chrgr_id['user_id'], 
            'correlation_id': new_chrgr_id['correlation_id'],
            'username': new_chrgr_id['username'], 
            'ip_addr': new_chrgr_id['ip_addr'], 
            'mac_addr': new_chrgr_id['mac_addr'],
            'assoc_edge_dev': new_chrgr_id['assoc_edge_dev'],
            'assoc_drone': new_chrgr_id['assoc_drone'],
            'is_active': new_chrgr_id['is_active'],
            'is_deleted': new_chrgr_id['is_deleted'],
            'created_by': new_chrgr_id['created_by'],
            'modified_by': new_chrgr_id['modified_by'],
            'created_tmsp': new_chrgr_id['created_tmsp'],
            'modified_tmsp': new_chrgr_id['modified_tmsp']}
            return jsonify({'result': output, 
            'tmsp': datetime.datetime.utcnow()})


    

    #PUT REQUESTS
    @app.route('/edgedev', methods=['PUT'])
    def put_edge_dev():
        edge = mongo.db.edge_devc
        user_id = 1
        correlation_id = "01"
        username = request.json['username']
        ip_addr = request.json['ip_addr']
        mac_addr = request.json['mac_addr']
        edge_dev_username = request.json['edge_dev_username']
        edge_dev_pass = request.json['edge_dev_pass']
        assoc_drones = request.json['assoc_drones']
        assoc_chrgs = request.json['assoc_chrgs']
        is_active = True
        is_deleted = False
        created_by = "user"
        modified_by = "user"
        created_tmsp = datetime.datetime.utcnow()
        modified_tmsp = "08-01-2022 03:15:10"
        
       
        edge_dev_id = edge.insert({'user_id': user_id, 
        'correlation_id': correlation_id, 'username': username,
        'ip_addr': ip_addr, 
        'mac_addr': mac_addr,
        'edge_dev_username':edge_dev_username,
        'edge_dev_pass':edge_dev_pass, 
        'assoc_drones': assoc_drones,
        'assoc_chrgs':assoc_chrgs,
        'is_active':is_active, 
        'is_deleted':is_deleted,
        'created_by':created_by,
        'modified_by':modified_by,
        'created_tmsp':created_tmsp,
        'modified_tmsp':modified_tmsp})

        new_edge_id = edge.find_one({'_id': edge_dev_id})

        output = {'user_id': new_edge_id['user_id'], 
        'correlation_id': new_edge_id['correlation_id'],
        'username': new_edge_id['username'], 
        'ip_addr': new_edge_id['ip_addr'], 
        'mac_addr': new_edge_id['mac_addr'],
        'edge_dev_username': new_edge_id['edge_dev_username'],
        'edge_dev_pass': new_edge_id['edge_dev_pass'],
        'assoc_drones': new_edge_id['assoc_drones'],
        'assoc_chrgs': new_edge_id['assoc_chrgs'],
        'is_active': new_edge_id['is_active'],
        'is_deleted': new_edge_id['is_deleted'],
        'created_by': new_edge_id['created_by'],
        'modified_by': new_edge_id['modified_by'],
        'created_tmsp': new_edge_id['created_tmsp'],
        'modified_tmsp': new_edge_id['modified_tmsp']}
        return jsonify({'result': output, 
        'tmsp': datetime.datetime.utcnow()})


        # username = request.json['username']
        # ip_addr = request.json['ip_addr']
        # mac_addr = request.json['mac_addr']
        # edge_dev_username = request.json['edge_dev_username']
        # edge_dev_pass = request.json['edge_dev_pass']
        # assoc_drones = request.json['assoc_drones']
        # assoc_chrgs = request.json['assoc_chrgs']
        # myquery = {
        #     "username" : "thirumurugan",
        #     "ip_addr" : "192.168.0.12",
        #     "mac_addr" : "2E:7Y:99:UU:00:AA",
        #     "edge_dev_username": "user",
        #     "edge_dev_pass": "123456",
        #     "assoc_drones": ["drone1","drone2"],
        #     "assoc_chrgs": ["chrg1","chrg2"]
        #     }
        
        #updated_val = 


    # DELETE REQUEST
    @app.route('/edgedev', methods=['DELETE'])
    def del_edge_dev():
        edge = mongo.db.edge_devc
        del_the_doc = { "correlation_id": {"$regex": "^01"} }
        del_succss = edge.delete_many(del_the_doc)
        return jsonify ({"result": "entry deleted successfully", "status code": 202})



    # Error Handler, for 404 status 
    @app.errorhandler(404)
    def not_found(error = None):
        message = {'message': 'resource not found ' + request.url + " please enter correct URL",
        'status': 404, 
        'tmsp': datetime.datetime.utcnow()}
        return message


    # @app.route('/errorlogger')
    # def error_logger():
      
    #     return 1 / 0

    # @app.route('/private')
    # def index():
    #     if oidc.user_loggedin:
    #         return 'Welcome %s' % oidc.user_getfield('email')
    #     else:
    #         return 'Not logged in'

    # @app.route('/login')
    # @oidc.require_login
    # def login():
    #     return 'Welcome %s' % oidc.user_getfield('email')

class Diagonistics():
    pass

class Analytics():
    pass

class Notification():
    pass

class Login():
    pass




if __name__ == '__main__':
    app.run(debug=True)
    Configuration()
    